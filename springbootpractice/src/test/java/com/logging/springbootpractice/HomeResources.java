package com.logging.springbootpractice;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeResources {

	@RequestMapping("/")
	public String home() {
		return "Springboot application";
	}
}
